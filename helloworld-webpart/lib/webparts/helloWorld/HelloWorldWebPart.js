var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import { Version } from '@microsoft/sp-core-library';
import { PropertyPaneTextField } from '@microsoft/sp-property-pane';
import { BaseClientSideWebPart } from '@microsoft/sp-webpart-base';
import styles from './HelloWorldWebPart.module.scss';
import * as strings from 'HelloWorldWebPartStrings';
import { SPComponentLoader } from '@microsoft/sp-loader';
require('../../../node_modules/bootstrap/dist/css/bootstrap.min.css');
var HelloWorldWebPart = /** @class */ (function (_super) {
    __extends(HelloWorldWebPart, _super);
    function HelloWorldWebPart() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    HelloWorldWebPart.prototype.onInit = function () {
        SPComponentLoader.loadScript('https://kit.fontawesome.com/8708c1ca53.js');
        return _super.prototype.onInit.call(this);
    };
    HelloWorldWebPart.prototype.render = function () {
        this.domElement.innerHTML = "\n      <div class=\"" + styles.helloWorld + "\">\n      <div class=\"container\">\n      <div class=\"card-group w-50\">\n        <div class=\"card bg-success m-1\">\n          <div class=\"card-body text-center\">\n            <i class=\"fas fa-home\"></i>\n          </div>\n        </div>\n        <div class=\"card bg-primary m-1\">\n          <div class=\"card-body text-center\">\n            <i class=\"fas fa-laptop-code\"></i>\n          </div>\n        </div>\n        <div class=\"card bg-danger m-1\">\n          <div class=\"card-body text-center\">\n            <i class=\"fas fa-school\"></i>\n          </div>\n        </div> \n      </div>\n      <div class=\"card-group w-50\">   \n        <div class=\"card bg-warning m-1\">\n          <div class=\"card-body text-center\">\n            <i class=\"fab fa-steam\"></i>\n          </div>\n        </div>\n        <div class=\"card bg-info m-1\">\n          <div class=\"card-body text-center\">\n            <i class=\"fas fa-bowling-ball\"></i>\n          </div>\n        </div>\n        <div class=\"card bg-light m-1\">\n          <div class=\"card-body text-center\">\n            <i class=\"fas fa-award\"></i>\n          </div>\n        </div>\n      </div>\n      </div>\n      </div>\n      ";
    };
    Object.defineProperty(HelloWorldWebPart.prototype, "dataVersion", {
        get: function () {
            return Version.parse('1.0');
        },
        enumerable: true,
        configurable: true
    });
    HelloWorldWebPart.prototype.getPropertyPaneConfiguration = function () {
        return {
            pages: [
                {
                    header: {
                        description: strings.PropertyPaneDescription
                    },
                    groups: [
                        {
                            groupName: strings.BasicGroupName,
                            groupFields: [
                                PropertyPaneTextField('description', {
                                    label: strings.DescriptionFieldLabel
                                })
                            ]
                        }
                    ]
                }
            ]
        };
    };
    return HelloWorldWebPart;
}(BaseClientSideWebPart));
export default HelloWorldWebPart;
//# sourceMappingURL=HelloWorldWebPart.js.map
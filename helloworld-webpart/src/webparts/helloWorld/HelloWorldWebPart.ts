import { Version } from '@microsoft/sp-core-library';
import {
  IPropertyPaneConfiguration,
  PropertyPaneTextField
} from '@microsoft/sp-property-pane';
import { BaseClientSideWebPart } from '@microsoft/sp-webpart-base';
import { escape } from '@microsoft/sp-lodash-subset';

import styles from './HelloWorldWebPart.module.scss';
import * as strings from 'HelloWorldWebPartStrings';

export interface IHelloWorldWebPartProps {
  description: string;
}

import { SPComponentLoader } from '@microsoft/sp-loader';

import * as jQuery from "jquery";
import * as bootstrap from "bootstrap";
require('../../../node_modules/bootstrap/dist/css/bootstrap.min.css');

export default class HelloWorldWebPart extends BaseClientSideWebPart<IHelloWorldWebPartProps> {

  protected onInit(): Promise<void> {
    SPComponentLoader.loadScript('https://kit.fontawesome.com/8708c1ca53.js');
    return super.onInit();
  }

  public render(): void {
    this.domElement.innerHTML = `
      <div class="${ styles.helloWorld }">
      <div class="container">
      <div class="card-group w-50">
        <div class="card bg-success m-1">
          <div class="card-body text-center">
            <i class="fas fa-home"></i>
          </div>
        </div>
        <div class="card bg-primary m-1">
          <div class="card-body text-center">
            <i class="fas fa-laptop-code"></i>
          </div>
        </div>
        <div class="card bg-danger m-1">
          <div class="card-body text-center">
            <i class="fas fa-school"></i>
          </div>
        </div> 
      </div>
      <div class="card-group w-50">   
        <div class="card bg-warning m-1">
          <div class="card-body text-center">
            <i class="fab fa-steam"></i>
          </div>
        </div>
        <div class="card bg-info m-1">
          <div class="card-body text-center">
            <i class="fas fa-bowling-ball"></i>
          </div>
        </div>
        <div class="card bg-light m-1">
          <div class="card-body text-center">
            <i class="fas fa-award"></i>
          </div>
        </div>
      </div>
      </div>
      </div>
      `;
  }

  protected get dataVersion(): Version {
    return Version.parse('1.0');
  }

  protected getPropertyPaneConfiguration(): IPropertyPaneConfiguration {
    return {
      pages: [
        {
          header: {
            description: strings.PropertyPaneDescription
          },
          groups: [
            {
              groupName: strings.BasicGroupName,
              groupFields: [
                PropertyPaneTextField('description', {
                  label: strings.DescriptionFieldLabel
                })
              ]
            }
          ]
        }
      ]
    };
  }
}
